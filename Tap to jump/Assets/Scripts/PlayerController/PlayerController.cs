﻿using UnityEngine;
namespace WarriorStudio
{
    public class PlayerController : MonoBehaviour
    {
        private float x;
        private Rigidbody2D rigi;

        public Joystick joystick;
        [SerializeField]
        private float speedMove;
        [SerializeField]
        private float jumpHeigt;
        private float maxJumpHeight = 1.0f;

        private bool canJump = true;
        private void Awake()
        {
            rigi = GetComponent<Rigidbody2D>();
        }
        private void Update()
        {
            MovePlayer();
        }
        private void OnCollisionEnter2D(Collision2D obj)
        {
            if (obj.collider.tag == "BG")
            {
                canJump = true;
            }
        }
        void MovePlayer()
        {
            x = joystick.Horizontal;
            if (x != 0 && Mathf.Abs(x) > 0.7f)
            {
                transform.position += new Vector3(x, 0, 0) * Time.deltaTime * speedMove;
            }
            if (rigi.velocity.y > maxJumpHeight)
            {
                canJump = false;
            }
        }
        public void Handle_Jump()
        {
            if (canJump)
                rigi.AddForce(transform.up * jumpHeigt, ForceMode2D.Impulse);
        }
    }
}
