﻿using UnityEngine;
namespace WarriorStudio
{
    public class MainGameController : MonoBehaviour
    {
        public static MainGameController api;

        [SerializeField]
        private PlayerController playerController;
        [SerializeField]
        private UIController uIController;
        private MainGameController()
        {

        }
        private void Awake()
        {
            if (api == null)
                api = this;
            DontDestroyOnLoad(api);

            uIController.Setup();
        }
        public void Handle_PlayerJump()
        {
            playerController.Handle_Jump();
        }
    }
}
