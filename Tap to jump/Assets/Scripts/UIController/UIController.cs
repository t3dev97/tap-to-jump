﻿using UnityEngine;
using UnityEngine.UI;
namespace WarriorStudio
{
    public class UIController : MonoBehaviour
    {
        [SerializeField]
        private Button btnJump;

        public void Setup()
        {
            btnJump.onClick.AddListener(() =>
            {
                HandleEvent_BtnJumpClick();
            });
        }
        private void HandleEvent_BtnJumpClick()
        {
            MainGameController.api.Handle_PlayerJump();
        }
    }
}
